<?php

namespace Chatter\Middleware;

use Chatter\Models\User as User; 

class Authentication {
    
    public function __invoke($request,$response,$next){
        
        $auth=$request->getHeader('Authorization');
        $_apikey=$auth[0];
        $apikey=trim($_apikey);
        
        $user=new User();
        if(!$user->authenticate($apikey)){
            
            $response->withStatus(401);
            
            return $response;
        }
        $response=$next($request,$response);
        
        return $response;
        
    }
}

?>