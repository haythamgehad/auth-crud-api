<?php
require '/../../../config/credentials.php';

use OAuth\Models\User;

$app->get('/'.$full_prefix.'/user', function ($request, $response, $args) {
    
    $user=new User();
    
    $users = $user->all();

    return $response->withStatus(200)->withJson($users);
});


$app->get('/'.$full_prefix.'/user/filter', function ($request, $response, $args) {
    
    $data=$request->getQueryParams();
    
    $user=new User();

    $users = $user->where($data)->get();

    return $response->withStatus(200)->withJson($users);

});

$app->post('/'.$full_prefix.'/user', function ($request, $response, $args) {
    
    $data=$request->getQueryParams();
    
    $user=new User();
    
    $data['password']=(isset($data['password']))?md5($data['password']):null;

    $result=$user->insert($data);

    if($result){
        
        $payload=['status'=>'success','error'=>'false' ];
        return $response->withStatus(201)->withJson($payload);
        
    } else {
        
        return $response->withStatus(400);
        
    }

});

$app->put('/'.$full_prefix.'/user', function ($request, $response, $args) {
    
    $data=$request->getQueryParams();
    
    $user=new User();
    
    if(isset($data['password'])){$data['password']=md5($data['password']);}

    $result=$user->where('username',$data['username'])->update($data);
    
    if($result){
        
        $payload=['status'=>'success','error'=>'false' ];
        return $response->withStatus(201)->withJson($payload);
        
    } else {
        
        return $response->withStatus(400);
        
    }

});


$app->delete('/'.$full_prefix.'/user', function ($request, $response, $args) {
    
    $data=$request->getQueryParams();
    
    $user=new User();
    
    if(isset($data['username'])){

    $result=$user->where('username',$data['username'])->delete();
    
    }
    if($result){
        
        $payload=['status'=>'success','error'=>'false' ];
        return $response->withStatus(201)->withJson($payload);
        
    } else {
        
        return $response->withStatus(400);
        
    }

});



?>