<?php
require '/../../../config/credentials.php';

use OAuth\Models\Client;

$app->get('/'.$full_prefix.'/client', function ($request, $response, $args) {
    
    $client=new Client();
    
    $clients = $client->all();

    return $response->withStatus(200)->withJson($clients);
});


$app->get('/'.$full_prefix.'/client/filter', function ($request, $response, $args) {
    
    $data=$request->getQueryParams();
    
    $client=new Client();

    $clients = $client->where($data)->get();

    return $response->withStatus(200)->withJson($clients);

});

$app->post('/'.$full_prefix.'/client', function ($request, $response, $args) {
    
    $data=$request->getQueryParams();
    
    $client=new Client();

    $result=$client->insert($data);

    if($result){
        
        $payload=['status'=>'success','error'=>'false' ];
        return $response->withStatus(201)->withJson($payload);
        
    } else {
        
        return $response->withStatus(400);
        
    }

});

$app->put('/'.$full_prefix.'/client', function ($request, $response, $args) {
    
    $data=$request->getQueryParams();
    
    $client=new Client();

    $result=$client->where('client_id',$data['client_id'])->update($data);
    
    if($result){
        
        $payload=['status'=>'success','error'=>'false' ];
        return $response->withStatus(201)->withJson($payload);
        
    } else {
        
        return $response->withStatus(400);
        
    }

});


$app->delete('/'.$full_prefix.'/client', function ($request, $response, $args) {
    
    $data=$request->getQueryParams();
    
    $client=new Client();
    
    if(isset($data['client_id'])){

    $result=$client->where('client_id',$data['client_id'])->delete();
    
    }
    if($result){
        
        $payload=['status'=>'success','error'=>'false' ];
        return $response->withStatus(201)->withJson($payload);
        
    } else {
        
        return $response->withStatus(400);
        
    }

});



?>