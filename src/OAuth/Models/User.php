<?php

namespace OAuth\Models;

class User extends \Illuminate\Database\Eloquent\Model
{
    protected $hidden=['password'];
    protected $fillable=['username'];
}