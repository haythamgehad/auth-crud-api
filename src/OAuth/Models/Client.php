<?php

namespace OAuth\Models;

class Client extends \Illuminate\Database\Eloquent\Model
{
    protected $hidden=['secret'];
    protected $fillable=['client_id','client_secret','redirect_uri'];
}