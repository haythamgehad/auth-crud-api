<?php

if(isset($_SERVER['REQUEST_URI']))
{   
    require 'config/credentials.php';

    $link=strtolower('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
    $path_version=false;
    
    foreach($versions as $version){
        
        $version_detect=strpos($link,'/'.$version.'/');
        if($version_detect!=false){
            $path_version=$version;
            break;
        }
    
    }
    
    if(!$path_version){
        
        exit('wrong version');

    }
    
    
    
    $full_prefix=$prefix.'/'.$version;
    $segment_start=strpos($link,$full_prefix)+strlen($full_prefix)+1;
    $is_input=strpos($link,'?');
    $segment_length=($is_input)?$is_input-$segment_start:strlen($link)-$segment_start;
    $segment=substr($link,$segment_start,$segment_length); 
    $segment_array = explode('/', $segment);
    $segments_count=count($segment_array);
    $path_segment=$segment_array[0];
    require 'src/OAuth/Routes/'.$path_segment.'.php';
} 


?>