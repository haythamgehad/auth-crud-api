<?php

require "vendor/autoload.php";
require "bootstrap.php";

use OAuth\Middleware\Logging as OAuthLogging;


$app= new \Slim\App();
$app->add(new OAuthLogging());

require 'routes.php';

$app->run();


?>